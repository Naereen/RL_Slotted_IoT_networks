### Learning_Slotted_IoT_networks

This repository contains the code which has been used in order to generate the simulation results for our article: 

Bonnefoi, R.; Besson, L.; Kaufmann, E.; Moy, C.; Palicot, J.  “Multi-Armed Bandit Learning in IoT Networks:Learning helps even in non-stationary settings”, CROWNCOM, May 2017

This article can be found on Springer. A version is also available at: https://hal.archives-ouvertes.fr/hal-01575419/document

### Short description 

The main files allow to evaluate the performances of some learning algorithms (UCB and Thompson Sampling) in a networks composed of IoT devices. The aim of this file is to evaluate the performance of learning algorithms when the number of devices which use the network increases.

### License

[MIT Licensed](https://mit-license.org/) (file [LICENSE.txt](LICENSE.txt)).