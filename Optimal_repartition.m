
function [D_ch]         = Optimal_repartition(p,S,D)

% This function computes the optimal repartition of dynamic end-devices in channels
% in which several static devices communicate with the base station
%
%   Inputs:
%   p           : probability that the end-devices (static and dynamic)
%                 send a packet to the base station
%   S           : repartition of static end-devices in channels
%   D           : Number of dynamic devices
%
%   Outputs:
%   D_ch        : repartition of dynamic end-devices in channels

%   Other m-files required: none
%   Subfunctions: none
%   MAT-files required: none

%   Authors  : R�mi BONNEFOI(1) - Lilian BESSON (1)(2)
%   (1)SCEE Research Team - CentraleSup�lec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%   (2)SequeL Research Team - Inria Lille
%   40, avenue du Halley, 59650 Villeneuve d'Ascq, FRANCE
%   Email : first.last@centralesupelec.fr
%
%   Last revision : 20/10/2017

    % bisection method
    %max_s       = max(S);
    D_temp          = -1;
    S_ite           = S;

    while min(D_temp)<0,
        
        % Initial value of lambda
        Nc          = length(S_ite>-1);
        lambda      = [0 10^100];
        f1          = sum((1/log(1-p))*(lambertw(lambda(1,1)*exp(1)./((1-p).^(S_ite(S_ite>-1)-1)))-1))-D;
        f2          = sum((1/log(1-p))*(lambertw(lambda(1,2)*exp(1)./((1-p).^(S_ite(S_ite>-1)-1)))-1))-D;

        % bisection
        while lambda(1,2)-lambda(1,1)>1*10^(-3),
            lambda_temp         = sum(lambda)/2;

            f                   = sum((1/log(1-p))*(lambertw(lambda_temp*exp(1)./((1-p).^(S_ite(S_ite>-1)-1)))-1))-D;

            if f>0,
                lambda(1,1)     = lambda_temp;
            else
                lambda(1,2)     = lambda_temp;
            end

        end

        D_temp                  = zeros(1,Nc);
        D_temp(S_ite>-1)        = (1/log(1-p))*(lambertw(lambda_temp*exp(1)./((1-p).^(S_ite(S_ite>-1)-1)))-1);

        S_ite(D_temp<0)         = -1;

    end

    D_ch            = round(D_temp);

end